# defines URL's view functions

from flask import current_app as app
from flask import render_template, url_for

@app.route('/')
def home():
    return render_template('home.html',
                            title="Flask Application Factory",
                            description="Better layout \
                                with Flask")